﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawning : MonoBehaviour {

    public PlayerHealth playerHealth;
    public GameObject pickup;
    public float spawnTime = 30f;
    public Transform[] spawnPoints;

    // Use this for initialization
    void Start()
    {

        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Instantiate(pickup, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
