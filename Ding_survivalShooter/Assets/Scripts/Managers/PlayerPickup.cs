﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickup : MonoBehaviour {

    public PlayerHealth playerHealth;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    

    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Pickup"))
        {
            GameObject thePlayer = GameObject.Find("Player");
            PlayerHealth playerHealth = thePlayer.GetComponent<PlayerHealth>();

            playerHealth.currentHealth += 25;

            playerHealth.healthSlider.value = playerHealth.currentHealth;

            Destroy(other.gameObject);
        }
       

    }
}
