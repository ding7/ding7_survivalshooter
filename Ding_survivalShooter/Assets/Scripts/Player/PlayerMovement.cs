﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 0.2f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;
    
                                                                                                 
    public float MouseSensitivity;


void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        playerRigidbody.MoveRotation(playerRigidbody.rotation * Quaternion.Euler(new Vector3(0, Input.GetAxis("Mouse X") * MouseSensitivity, 0)));
        playerRigidbody.MovePosition(transform.position + (transform.forward * v * speed) + (transform.right * h * speed));

     


        if (Input.GetKeyDown("left shift"))
        {
            
            speed = 0.4f;
            
        }
        if (Input.GetKeyUp("left shift"))
        {

            speed = 0.2f;

        }

    }
    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

      

        Animating(h, v);

    }



    

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }

}

